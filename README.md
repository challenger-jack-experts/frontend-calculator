# Começando com o Create React App
Este projeto foi iniciado com o Visite o [Create React App](https://github.com/facebook/create-react-app) para obter mais informações.

## Scripts Disponíveis
No diretório do projeto, você pode executar:

```
npm start
```

Executa o aplicativo no modo de desenvolvimento.
Abra http://localhost:3000 para visualizá-lo no navegador.
A página será recarregada se você fizer edições.
Você também verá quaisquer erros de lint no console.

```
npm test
```

Inicia o executor de teste no modo interativo de observação.
Consulte a seção sobre [execução de testes](https://create-react-app.dev/docs/running-tests) para obter mais informações.

```
npm run build
```

Compila o aplicativo para produção na pasta build.

Ele empacota corretamente o React no modo de produção e otimiza a construção para obter o melhor desempenho.

O build é minificado e os nomes dos arquivos incluem os hashes.
Seu aplicativo está pronto para ser implantado!

Consulte a seção sobre [implantação](https://create-react-app.dev/docs/deployment/) para obter mais informações.

```
npm run eject
```

Nota: esta é uma operação unilateral. Depois de ejetar, você não pode voltar atrás!

Se você não estiver satisfeito com a ferramenta de construção e as escolhas de configuração, poderá ejetar a qualquer momento. Esse comando removerá a única dependência de construção do seu projeto.

Em vez disso, ele copiará todos os arquivos de configuração e as dependências transitivas (webpack, Babel, ESLint etc.) diretamente para o seu projeto, para que você tenha controle total sobre eles. Todos os comandos, exceto eject, ainda funcionarão, mas apontarão para os scripts copiados para que você possa ajustá-los. Neste ponto, você está por conta própria.

Você não precisa usar o eject nunca. O conjunto de recursos selecionados é adequado para implantações pequenas e médias, e você não deve se sentir obrigado a usar esse recurso. No entanto, entendemos que essa ferramenta não seria útil se você não pudesse personalizá-la quando estiver pronto para isso.

## Saiba Mais
Você pode aprender mais na [documentação do Create React App](https://create-react-app.dev/docs/getting-started/).

Para aprender React, confira a [documentação do React](https://react.dev)