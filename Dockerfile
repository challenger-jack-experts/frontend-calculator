FROM node:current-alpine3.16

WORKDIR /app

COPY . /app

EXPOSE 3000

CMD ["npm", "start"]